Required:
- Python 2.7
- Scrapy installed
- Selenium installed
- Chrome webdriver

*** Install required libs ***
pip install -r requirements.txt

==========================================

- Extract this package
- Config params in config.json file


- Open command window(Windows) or Terminal(Linux) then move to this folder
- Run command: 
For windows:  

+ Scrape conference, panels, individuals, individual role at panel, companies, individual role at company
  
  scrapy crawl bizzabo -a url=https://www.coindesk.com/events/consensus-2018/agenda/

+ Scrape individual email:
  
  scrapy crawl speaker -a url=https://www.coindesk.com/events/consensus-2018/agenda/

+ Mapping video to panel

  scrapy crawl video -a url=https://www.coindesk.com/events/consensus-2018/videos/

+ Scrape sponsors, company role at conference

  scrapy crawl sponsor -a url=https://www.coindesk.com/events/consensus-2018/sponsors/


- That's all




