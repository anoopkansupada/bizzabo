import string
import zipfile
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import ElementNotVisibleException
import time
import json
import re
import random
import base64
from selenium.webdriver.common.proxy import *

from scrapy.utils.project import get_project_settings
from fake_useragent import UserAgent

import smtplib
import tldextract

settings = get_project_settings()
with open('config.json', 'r') as ip:
    _config = json.loads(ip.read())            
    config = _config[_config['environment']] 	

driver = None

def init_web_driver(proxyOpt = None):   
    global driver 
    
    # Firefox
    if proxyOpt == 1:
        driver = webdriver.Firefox()

    # Chrome
    elif proxyOpt == 2:
        chromeOptions = webdriver.ChromeOptions()   
        prefs = {"download.default_directory" : config['download_folder']}
        chromeOptions.add_experimental_option("prefs",prefs)  
        #chromeOptions.add_argument("--start-maximized") 
        
        path_to_chromedriver = config['path_to_chromedriver'] # change path as needed
        driver = webdriver.Chrome(executable_path = path_to_chromedriver, chrome_options=chromeOptions)

    elif proxyOpt == 3:     
        proxy_add = "socks5://localhost:8788"

        chromeOptions = webdriver.ChromeOptions()   
        prefs = {"download.default_directory" : config['download_folder']}
        chromeOptions.add_experimental_option("prefs",prefs)

        ##### full screen
        #chromeOptions.add_argument("--start-maximized") 

        ##### proxy auth
        #chromeOptions.add_extension(proxyauth_plugin_path)

        ##### proxy
        chromeOptions.add_argument('--proxy-server=%s' % proxy_add)                
        path_to_chromedriver = config['path_to_chromedriver'] # change path as needed
        driver = webdriver.Chrome(executable_path = path_to_chromedriver, chrome_options=chromeOptions)

    return driver

def close_web_driver():
    if driver:
        driver.close()

""" Wait for element """
def wait_for_element(driver, selector, timeout=10):
    try:
        wait = WebDriverWait(driver, timeout).until(
            EC.presence_of_element_located((By.CSS_SELECTOR , selector))
            )   

        return driver.find_element_by_css_selector(selector)
    except:
        print ('Element with selector "%s" not visible' % selector)
        return False

""" Wait for element xpath"""
def wait_for_element_xpath(driver, xpath, timeout=10):  
    try:
        wait = WebDriverWait(driver, timeout).until(
            EC.presence_of_element_located((By.XPATH , xpath))
            )    

        return driver.find_element_by_xpath(xpath)
    except:
        print ('Element with xpath "%s" not visible' % xpath)
        return False
        
def saveUrl(file, url):        	
    bSaved = False
    url = url.strip()
    f = open(file, 'r+')
    lines = f.readlines()
    f.seek(0)
    f.truncate()
    
    for line in lines: 
        line = line.strip()
        f.write(line.encode('utf-8') + '\n')
        if url == line:
            bSaved = True                
                                                  
    if bSaved == False:
        f.write(url.encode('utf-8') + '\n')
        
    f.close()
    
    return bSaved
    
def sendemail(from_addr, to_addr_list, cc_addr_list,
              subject, message,
              login, password,
              smtpserver='smtp.gmail.com:587'):
    header = 'From: %s' % from_addr
    header += '\nTo: %s' % ','.join(to_addr_list)
    if len(cc_addr_list):
        header += '\nCc: %s' % ','.join(cc_addr_list)
    header += '\nSubject: %s' % subject
    message = header + message
             
    server = smtplib.SMTP(smtpserver)
    server.starttls()
    server.login(login,password)
    problems = server.sendmail(from_addr, to_addr_list, message)
    server.quit()
    
def sendAlert(_message=None):	
    if config['email']['allow_send'] == 1:
        login = config['email']['user']
        passwd =  config['email']['pass']
        
        if _message == None:            
            _subject = config['vm'] + " - " + config['email']["subject"]
        else:
            _subject = config['vm'] + " - " + _message
        
        sendemail(
            from_addr = config['email']['user'], 
            to_addr_list = config['email']["to_addr_list"], 
            cc_addr_list = config['email']["cc_addr_list"],
            subject = _subject, 
            message = config['email']["message"],                
            login = login,
            password = passwd
        )
    else:
        pass

def get_domain(url):     
    list = tldextract.extract(url)
    domain_name = list.domain + '.' + list.suffix
    return domain_name
    