# -*- coding: utf-8 -*-
#!/bin/sh
import sys
import re
import string
import scrapy
import csv
import logging
import json
import urllib
from scrapyx.utils import * 
from scrapy.selector import Selector
from scrapyx.items import ArticleItem
from scrapy.http import TextResponse 
from scrapy import Request, FormRequest
import scrapy
import time
import datetime
import pytz
import dateutil.parser as dateparser

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import NoSuchElementException    
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys

from PIL import Image
from time import sleep
from datetime import date
from random import uniform, randint

import ast
#from scrapyx.postgres import Postgres
from scrapyx.mysql import * 

try:
    from pyvirtualdisplay import Display
except:
    print ("Cannot import Display")

from airtable import Airtable

class CollectSpider(scrapy.Spider):
    name = "collect"
    allowed_domains = []
    start_urls = []    
    config = []        
    driver = None    
    base_key = None
    api_key = None   

    def __init__(self, proxy_opt = None, prod_name = None, input_file = None, *args, **kwargs):
        super(CollectSpider, self).__init__(*args, **kwargs)
        with open('config.json', 'r') as ip:
            _config = json.loads(ip.read())            
            self.config = _config[_config['environment']]
            
        self.allowed_domains.append(self.config['domain'])
        self.start_urls.append(self.config['url'])    

        self.base_key = self.config['airtable_base_key']
        self.api_key = self.config['airtable_api_key']

        if proxy_opt != None:
          self.proxyOpt = int(proxy_opt)   
          self.driver = init_web_driver(self.proxyOpt)
        
        try:
            display = Display(visible=0, size=(800, 600))
            display.start()
        except:
            pass    

    def parse(self, response):
      
      self.driver.get(self.config['url'])

      wait_for_element_xpath(self.driver, '//div[@name="bizzabo-web-agenda"]')

      #self.wait_between(5.0, 7.0)

      iframes = self.driver.find_elements_by_tag_name('iframe')      

      iframe = None
      for i in iframes:
        if "Bizzabo-iFrameResizer-agenda-" in i.get_attribute('id'):
          iframe = i

      print iframe

      self.driver.switch_to_frame(iframe)

      wait_for_element_xpath(self.driver, '//div[contains(@class,"session-list-container")]')         

      # Scroll to more button
      day_containers = self.driver.find_elements_by_xpath('//div[@class="session-day-container"]')
      self.driver.execute_script("arguments[0].scrollIntoView();", day_containers[-1])      

      isMore = True
      while(isMore):
        more_btn = wait_for_element_xpath(self.driver, '//button[contains(@class,"load-more-button")]')
        if more_btn:          
          # Click on more button
          more_btn.click()
          
          # Scroll to more button
          day_containers = self.driver.find_elements_by_xpath('//div[@class="session-day-container"]')
          self.driver.execute_script("arguments[0].scrollIntoView();", day_containers[-1])          
          
        else:
          isMore = False

      # Parse content here
      responsex = TextResponse(url=self.driver.current_url, body=self.driver.page_source, encoding='utf-8')

      # Get general infos
      script_basic_info = responsex.xpath('.//script[contains(text(), "startDate")]//text()').extract_first()
      matches1 = re.findall('.*"startDate"\:\s"(.*?)"\,.*?"location".*?"endDate"\:\s"(.*?)"\,.*?"offers".*', script_basic_info.replace('\n', ''))
      if matches1:
        conf_start_date = datetime.datetime.strptime(matches1[0][0].split(' ')[0], '%Y-%m-%d').strftime('%m/%d/%Y')
        conf_end_date = datetime.datetime.strptime(matches1[0][1].split(' ')[0], '%Y-%m-%d').strftime('%m/%d/%Y')
        print conf_start_date, conf_end_date

      script_text = responsex.xpath('.//script[contains(text(), "window.__INITIAL_STATE__")]//text()').extract_first()
      matches = re.findall('.*\,"time-zone-id"\:"(.*?)"\,"unique-name".*', script_text)
      if matches:
        tz = matches[0]     
        print tz   

      matches2 = re.findall('.*\,"city"\:"(.*?)"\,"country"\:"(.*?)"\,"display-address"\:"(.*?)"\,"latitude".*', script_text)
      if matches2:
        city = matches2[0][0]
        country = matches2[0][1]
        address = matches2[0][2]    
        print city, country, address          

      # Add to table 'CONFERENCE'
      _air_confer_table = Airtable(self.base_key, 'CONFERENCE', api_key=self.api_key)

      conference_name = responsex.xpath('.//meta[@property="og:title"]//@content').extract_first()
      conference_url = responsex.xpath('.//meta[@property="og:url"]//@content').extract_first()
      conference_desc = responsex.xpath('.//meta[@property="og:description"]//@content').extract_first()    

      days = responsex.xpath('.//button[contains(@class, "day-elems") and not(contains(@class, "all-days"))]//@value').extract()
      day_index = 0

      #conference_start_date = str(datetime.datetime.utcfromtimestamp(int(days[0])/1000).strftime('%m/%d/%Y %H:%M:%S %Z%z'))
      #conference_end_date = str(datetime.datetime.utcfromtimestamp(int(days[-1])/1000).strftime('%m/%d/%Y %H:%M:%S %Z%z'))        

      conf = []
      confs = _air_confer_table.search('Name', str(conference_name))
      #print conf
      conf_fields = {
          'Notes': conference_desc,
          'Name': conference_name,
          'Year': conf_start_date.split('/')[-1],
          'Start Date': conf_start_date,
          'End Date': conf_end_date,
          'URL': conference_url,
          'Agenda/People URL': conference_url + 'agenda/',
          'City': city,
          'Country': country
        }
      if not confs:        
        conf = _air_confer_table.insert(conf_fields)
      else:
        conf = confs[0]
        #_air_confer_table.update(conf['id'], conf_fields)
        

      conf_ID = conf['fields']['ID']
      conf_idx = conf['id']
            
      # Insert table 'PANELS '      
      day = None
      user_list = []

      for div in responsex.xpath('.//div[contains(@class, "session-day")]//div[@class="col-xs-12"]//div[@class="session-day-container"]'):
        if div.xpath('.//span[@class="day-title"]'):
          day = days[day_index]
          day_index += 1

        #if div.xpath('.//div[contains(@class, "speaker-name")]'):

        locations = div.xpath('.//div[contains(@class,"session-location")]//text()').extract()
        panel_stage = ''
        if locations:
          panel_stage = ' '.join([x.replace('\n', '').strip() for x in locations if x])
        
        _topic = div.xpath('.//div[contains(@class, "session-name")]//text()').extract_first().strip()
        topic = _topic.encode('utf-8').title()

        _start_time = div.xpath('.//span[contains(@class, "start-time")]//text()').extract_first()
        start_time = _start_time.strip()

        _end_time = div.xpath('.//span[contains(@class, "end-time")]//text()').extract()[-1]
        end_time = _end_time.replace('-', '').strip()

        # Insert table 'PANELS'
        _air_panels_table = Airtable(self.base_key, 'PANELS', api_key=self.api_key)

        _search_term = str(topic) + str(' @ "') + str(conf_ID) + '"'
        search_term = _search_term.replace("'", r"\'")
        panels = _air_panels_table.search('Name', search_term, fields = ['Name'])
        
        if not panels:
          """
          panel_fields = {
            'Topic': topic,
            'Date': str(datetime.datetime.utcfromtimestamp(int(time.time())).strftime('%m/%d/%Y')),
            'Link to Conference': [conf_idx],
            'Start Time': str(datetime.datetime.utcfromtimestamp(int(day)/1000).strftime('%m/%d/%Y')),
            'End Time': str(datetime.datetime.utcfromtimestamp(int(day)/1000).strftime('%m/%d/%Y')),
          }
          """

          day_format = str(datetime.datetime.utcfromtimestamp(int(day)/1000).strftime('%m/%d/%Y'))
          start_time_string = day_format + ' ' + start_time
          end_time_string = day_format + ' ' + end_time

          _date = datetime.datetime.strptime(day_format, '%m/%d/%Y').strftime('%Y-%m-%d')

          panel_fields = {
            'Topic': topic,
            'Date': _date,
            'Link to Conference': [conf_idx],
            'Start Time': self.convert_time(start_time_string), #str(datetime.datetime.utcfromtimestamp(int(day)/1000).strftime('%m/%d/%Y')),
            'End Time': self.convert_time(end_time_string) #str(datetime.datetime.utcfromtimestamp(int(day)/1000).strftime('%m/%d/%Y')),              
          }

          if panel_stage != '':
            panel_fields.update({'Stage': panel_stage})

          panel = _air_panels_table.insert(panel_fields)
        else:
          panel = panels[0]     

        #self._exit()

        if div.xpath('.//div[contains(@class, "speakers-container")]//div[contains(@class,"slick-track")]//div[contains(@class,"slick-slide")]'):
          for speaker_info in div.xpath('.//div[contains(@class, "speakers-container")]//div[contains(@class,"slick-track")]//div[contains(@class,"slick-slide")]'):
            item = {}
            item['topic'] = topic
            item['panel'] = panel['id']
            speaker_name = speaker_info.xpath('.//div[contains(@class, "speaker-name")]//text()').extract_first()
            item['speaker_name'] = speaker_name.replace('Mr.', '').replace('Ms.', '')\
              .replace('Mrs.', '').replace('Miss.', '').replace('Dr. Dr.', 'Dr.').strip().encode('utf-8')

            speaker_company = speaker_info.xpath('.//div[contains(@class, "speaker-company")]//text()').extract_first()
            item['speaker_company'] = speaker_company.strip()

            speaker_position = speaker_info.xpath('.//div[contains(@class, "speaker-position")]//text()').extract_first()
            item['speaker_position'] = speaker_position.strip()

            profile_url = speaker_info.xpath('.//a[contains(@class, "agenda-flex")]//@href').extract_first()
            item['profile_url'] = "https://events.bizzabo.com" + profile_url
            
            item['start_time'] = start_time
            
            item['end_time'] = end_time
            item['day'] = str(datetime.datetime.utcfromtimestamp(int(day)/1000).strftime('%m/%d/%Y'))

            user_list.append(item)

            """ Testing insert individual
            _air_indiv_table = Airtable(self.base_key, 'INDIVIDUALS', api_key=self.api_key)
            indivs = _air_indiv_table.search('Name', item['speaker_name'])
            if not indivs:
              name_parts = item['speaker_name'].split(' ')
              indiv_fields = {
                'F. Name': ' '.join(name_parts[:-1]),
                'L. Name': name_parts[-1],
                'Pic': [{'url': 'https://res.cloudinary.com/bizzaboprod/image/upload/c_crop,g_custom/w_60,h_60,c_fill,g_face,f_auto,q_auto:best/fhkjeai7hjawtq7genav.jpg'}],                                
              }
              indiv = _air_indiv_table.insert(indiv_fields)

            Testing insert individual"""

      # print user_list
      """ Scrape individual details """
      scraped_users = []
      new_user_list = []      
      
      if len(user_list):        
        for x in user_list:      
          profile_url = x['profile_url']
          if profile_url in scraped_users:                        
            continue

          scraped_users.append(profile_url)

          bLoop = False
          while not bLoop:
            self.driver.get(profile_url)          
            bLoop = wait_for_element_xpath(self.driver, '//div[@class="speaker-view"]', 10)

          responsey = TextResponse(url=self.driver.current_url, body=self.driver.page_source, encoding='utf-8')

          x['company_role'] = ''
          if responsey.xpath('.//div[contains(@class, "speaker-position")]'):
            company_role = responsey.xpath('.//div[contains(@class, "speaker-position")]//text()').extract_first()
            if company_role:
              x['company_role'] = company_role.strip()

          x['speaker_bio'] = ''
          if responsey.xpath('.//div[contains(@class, "speaker-bio-wrapper")]'):
            speaker_bio = responsey.xpath('.//div[contains(@class, "speaker-bio-wrapper")]//div[contains(@class, "speaker-bio")]//text()').extract_first()
            if speaker_bio:
              x['speaker_bio'] = speaker_bio.strip().encode('utf-8')

          x['speaker_img'] = ''
          if responsey.xpath('.//div[contains(@class, "speaker-image-wrapper")]'):
            speaker_img = responsey.xpath('.//div[contains(@class, "speaker-image-wrapper")]//img[@class="speaker-image-stub"]//@src').extract_first()
            if speaker_img:
              x['speaker_img'] = speaker_img

          x['linkedin'] = ''
          x['twitter'] = ''
          social_links = responsey.xpath('.//div[contains(@class, "speaker-socials")]//div[@class="speaker-social"]//a//@href').extract()
          if len(social_links):
            for soc_link in social_links:
              if 'linkedin.com' in soc_link:
                x['linkedin'] = soc_link
              if 'twitter.com' in soc_link:
                x['twitter'] = soc_link

          new_user_list.append(x)          
          #print x
      """ End scrape individual details """
          
      #print new_user_list   

      # Insert to table 'INDIVIDUALS'  
      _air_indiv_table = Airtable(self.base_key, 'INDIVIDUALS', api_key=self.api_key)
      inserted_user_list = {}
      for p in new_user_list:
        _speaker_name = str(p['speaker_name']).replace("'", r"\'")
        indivs = _air_indiv_table.search('Name', _speaker_name)
        if not indivs:
          name_parts = p['speaker_name'].split(' ')
          indiv_fields = {
            'F. Name': ' '.join(name_parts[:-1]),
            'L. Name': name_parts[-1],
            'Pic': [{'url': p['speaker_img'] if 'speaker_img' in p.keys() else ''}],
            'Linkedin': p['linkedin'] if 'linkedin' in p.keys() else '',
            'Twitter': p['twitter'] if 'twitter' in p.keys() else '',
            'Biography': p['speaker_bio'] if 'speaker_bio' in p.keys() else '',    
            'Individual Link': p['profile_url']
          }
          indiv = _air_indiv_table.insert(indiv_fields)
          indiv_id = indiv['id']
        else:
          # Update person
          indiv_id = indivs[0]['id']
          update_fields = {
            'Pic': [{'url': p['speaker_img'] if 'speaker_img' in p.keys() else ''}],
            'Individual Link': p['profile_url']
          }
          update = _air_indiv_table.update(indiv_id, update_fields)

        inserted_user_list[p['profile_url']] = indiv_id

      # Insert person to 'INDIVIDUAL ROLE AT PANEL'
      _air_role_panel_table = Airtable(self.base_key, 'INDIVIDUAL ROLE AT PANEL', api_key=self.api_key)
      for x in user_list:
        panel_id = x['panel']
        indiv_id = inserted_user_list[x['profile_url']] if x['profile_url'] in inserted_user_list.keys() else ''
        
        role_at_panel_fields = {
          'Link to Panel': [panel_id],
          'Link to Person': [indiv_id]          
        }

        _speaker_position = x['speaker_position'] if x['speaker_position'] != '' else 'Moderator'

        role_at_panel_fields.update({'Role': [_speaker_position]})

        _person_name = str(x['speaker_name']).replace("'", r"\'")
        if ',' in _person_name:
          _person_name = '"' + _person_name + '"'
        indiv_role_at_panel_s = _air_role_panel_table.search('Link to Person', _person_name)
        #print x, role_at_panel_fields, indiv_role_at_panel_s
        iCnt = 0
        if len(indiv_role_at_panel_s):          
          for i in indiv_role_at_panel_s:
            print str(i[u'fields'][u'Link to Panel'][0]), ' ---- ', str(panel_id)
            if str(i[u'fields'][u'Link to Panel'][0]) == str(panel_id):
              iCnt += 1                      

        if iCnt == 0:
          # Insert to 'INDIVIDUAL ROLE AT PANEL'
          inserted = _air_role_panel_table.insert(role_at_panel_fields)

      """ Insert 'COMPANIES' """

      _air_company_table = Airtable(self.base_key, 'COMPANIES', api_key=self.api_key)

      user_with_companyid = []
      for x in user_list:
        company_name = x['speaker_company']
        company_name = company_name.replace(u"\u2019", "'").encode('utf-8')
        insert_com_fields = {
          'Name': company_name
        }

        print company_name
        
        company_search_term = company_name.replace("'", r"\'")
        #if ',' in company_search_term:
        #  company_search_term = '"' + company_search_term + '"'

        companies = _air_company_table.search('Name', company_search_term)
        if not len(companies):          
          inserted_company = _air_company_table.insert(insert_com_fields)
          company_id = inserted_company['id']
        else:
          company_id = companies[0]['id']

        x.update({'company_id': company_id})
        user_with_companyid.append(x)

      """ End Insert 'COMPANIES' """

      """ Insert 'INDIVIDUAL ROLE AT COMPANY' """
      _air_role_at_company_table = Airtable(self.base_key, 'INDIVIDUAL ROLE AT COMPANY', api_key=self.api_key)
      for x in user_with_companyid:        
        indiv_id = inserted_user_list[x['profile_url']] if x['profile_url'] in inserted_user_list.keys() else ''
        company_id = x['company_id']

        role_at_company_fields = {
          'Link to Individual': [indiv_id],
          'Link to Company': [company_id],
          'Role': 'Employee'
        }

        if 'company_role' not in x.keys():
          if x['company_role'] != '':
            role_at_company_fields.update({'Official Title': x['company_role']})

        _person_name = str(x['speaker_name']).replace("'", r"\'")
        if ',' in _person_name:
          _person_name = '"' + _person_name + '"'

        indiv_role_at_company_s = _air_role_at_company_table.search('Link to Individual', _person_name)
        
        iCnt = 0
        if len(indiv_role_at_company_s):          
          for i in indiv_role_at_company_s:
            print str(i[u'fields'][u'Link to Company'][0]), ' ---- ', str(company_id)
            if str(i[u'fields'][u'Link to Company'][0]) == str(company_id):
              iCnt += 1                      

        if iCnt == 0:
          # Insert to 'INDIVIDUAL ROLE AT COMPANY'
          inserted = _air_role_at_company_table.insert(role_at_company_fields)

      """ End Insert 'INDIVIDUAL ROLE AT COMPANY' """
    

    def convert_time(self, time_string, timezone = 'America/New_York'):
      #time_string = time_string + ":"
      datetime_object = datetime.datetime.strptime(time_string, '%m/%d/%Y %I:%M %p')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return
    
    def wait_between(self, a, b):
      rand=uniform(a, b) 
      sleep(rand)

    def closed(self, reason = None):   
      if reason:
        print reason
      
      sendAlert()
      if self.driver:
        self.driver.close()

    def _exit(self):
      self.closed()
      sys.exit()
