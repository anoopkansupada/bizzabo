# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import scrapy
import sys
import csv
#import MySQLdb
import hashlib
from scrapy.exceptions import DropItem
from scrapy.http import Request
from scrapy.utils.project import get_project_settings
from scrapy.pipelines.files import FilesPipeline
from scrapy.pipelines.images import ImagesPipeline
import json

class CustomFilesPipeline(FilesPipeline):  
	#Name download version
	def file_path(self, request, response=None, info=None):
		item=request.meta['item'] # Like this you can use all from item, not just url.
		file_path = request.url.replace(item['baseUrl'], '')
				
		returnPath = 'docs/' + file_path
		return returnPath   
		
	def get_media_requests(self, item, info):
		for file_url in item['file_urls']:
			if file_url:
				yield scrapy.Request(file_url, meta={'item': item})
		
	def item_completed(self, results, item, info):
		file_paths = [x['path'] for ok, x in results if ok]
		if not file_paths:
			raise DropItem("Item contains no files")
		item['file_paths'] = file_paths
		return item
		
		
	
		